/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sendstatus;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.Timer;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;

/**
 *
 * @author Personal
 */
public class SendStatus implements ActionListener{    

    /**
     * @param args the command line arguments
     */
    
    JFrame f;
    static JTextField t, t1, p, p1, s;
    JButton r, c;
    JList list;
    static DefaultListModel listModel;
    
    private static String list_count;
    private static String printer_on;
    private static String terminal_id;
    
    private static String path1 = "C:/Users/Personal/Documents/NetBeansProjects/SendStatus/dist/";
    //private static String path1 = "C:/Documents and Settings/Admin/Рабочий стол/SendStatus/dist/";
    
    private static String currentState;
    
    public SendStatus() {
        
        listModel = new DefaultListModel();
        
        f=new JFrame("Status manager");
        t = new JTextField("terminal_id");
        t1 = new JTextField();
        p = new JTextField("total_pages");
        p1 = new JTextField();
        s = new JTextField("Send status");                
        r = new JButton("Refresh");
        c = new JButton("Clear");
        list = new JList(listModel);
        
        Font font = t.getFont().deriveFont((float)60);
        Font font2 = t.getFont().deriveFont((float)20);
        
        t.setBounds(15, 15, 115, 100);
        t.setFont(font2);
        t.setEditable(false);
        
        t1.setBounds(130, 15, 115, 100);
        t1.setFont(font2);
        t1.setEditable(false);        
        
        p.setBounds(15, 115, 115, 100);
        p.setFont(font2);
        p.setEditable(false);
        
        p1.setBounds(130, 115, 115, 100);
        p1.setFont(font2);
        p1.setEditable(false);
        
        s.setBounds(15, 215, 115, 100);
        s.setFont(font2);
        s.setEditable(false);
        
        r.setBounds(245, 140, 115, 60);
        r.setFont(font2);
        
        c.setBounds(365, 140, 115, 60);
        c.setFont(font2);
        
        JPanel panel = new JPanel(new BorderLayout());
        
        panel.setBounds(140, 225, 600, 400);       
        
        list.setBounds(140, 225, 600, 400);
        list.setLayoutOrientation(JList.VERTICAL);
        list.setSelectionMode(0);
                      
        JScrollPane listScroller = new JScrollPane(list);
        listScroller.setPreferredSize(new Dimension(100, 100));
        
        panel.add(listScroller);
        
        f.add(t);
        f.add(t1);
        f.add(p);
        f.add(p1);
        f.add(s);
        f.add(r);
        f.add(c);
        f.add(panel);
        f.setLayout(null);
        f.setVisible(true);
        f.setSize(800, 800);
        f.setResizable(false);        
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        c.addActionListener(this);
        r.addActionListener(this);
        
        updateInfoOnGUI();
        
    }
    
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == c) {
            listModel.clear();
        }
        if (e.getSource() == r) {            
            try {
                refreshPageCounter();
                p1.setText("140");
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SendStatus.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public static void main(String[] args) throws IOException, InterruptedException {
        // TODO code application logic here                               
        
    ScheduledExecutorService execService = Executors.newScheduledThreadPool(3);

    execService.scheduleAtFixedRate(new Runnable() {
        public void run() {
            try {
                getInfoFromFile();
                startSending();
            } catch (IOException ex) {
                Logger.getLogger(SendStatus.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InterruptedException ex) {
                Logger.getLogger(SendStatus.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }, 0L, 60L, TimeUnit.SECONDS);
        
     EventQueue.invokeLater(new Runnable() {            
        @Override
        public void run() {
            new SendStatus();        
        }
     });
//        getInfoFromFile();
//        
//        startSending();
        
    }

    private static void getInfoFromFile() throws IOException, InterruptedException {
        try (BufferedReader br = new BufferedReader(new FileReader("page_counter.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                list_count = sCurrentLine;
            }

	} catch (IOException e) {
            e.printStackTrace();
	}
        
         try (BufferedReader br = new BufferedReader(new FileReader("terminal_id.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                terminal_id = sCurrentLine;
            }

	} catch (IOException e) {
            e.printStackTrace();
	}                 
         
        checkPrinterStatus();
        
        try (BufferedReader br = new BufferedReader(new FileReader("printer_on.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                printer_on = sCurrentLine;
            }

	} catch (IOException e) {
            e.printStackTrace();
	}        
    }
    
    private static void startSending() {
         HttpClient httpClient = new DefaultHttpClient();

    try {
        HttpPost request = new HttpPost("http://daiyn.com/terminal_status_getter.php");
        StringEntity params =new StringEntity("json-string={\"page_counter\":\""+list_count+"\",\"printer_on\":\""+printer_on+"\",\"terminal_id\":\""+terminal_id+"\"}");        
        request.addHeader("content-type", "application/x-www-form-urlencoded");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);

        // handle response here...               
        
        org.apache.http.util.EntityUtils.consume(response.getEntity());                
    } catch (Exception ex) {
        // handle exception here
    } finally {
        httpClient.getConnectionManager().shutdown();
    }        
    
    }

    private static void checkPrinterStatus() throws IOException, InterruptedException {
        boolean isPrinterOn = false;
        
        try {
        Process process = new ProcessBuilder("sample.exe").start();
        //TimeUnit.SECONDS.sleep(3);
        
        process.destroy();
        } catch (IOException ex) {
            System.out.println("exception" + ex.toString());
        }
        
        
        
        try (BufferedReader br = new BufferedReader(new FileReader("usbinfo.txt"))) {

            String sCurrentLine;
            String temp = "";
            while ((sCurrentLine = br.readLine()) != null) {               
                System.out.println(sCurrentLine);                
                 temp += sCurrentLine+"\n";
                if (sCurrentLine.equals("USB\\VID_04A9&PID_26DA\\0000A393DDAE")) {
                    isPrinterOn = true;
                    break;
                }
            }
            System.out.println(temp);
            try(PrintWriter out = new PrintWriter("log.txt" )){
                           out.println(temp);                
                    }   

            if (isPrinterOn) {
                try(PrintWriter out = new PrintWriter("printer_on.txt" )){
                           out.println("1");                           
                    }   
            } else {
                try(PrintWriter out = new PrintWriter("printer_on.txt" )){
                           out.println("0");
                    }   
            }
            
	} catch (IOException e) {
            e.printStackTrace();
	}
    }
    
    private void refreshPageCounter() throws FileNotFoundException {
        try(PrintWriter out = new PrintWriter("page_counter.txt" )){
                           out.println("140");
                    }   
    }
 
    private static void updateInfoOnGUI() {        
         
        t1.setText("#"+terminal_id);
        p1.setText(""+list_count);
    
    ScheduledExecutorService execService1 = Executors.newScheduledThreadPool(3);

    execService1.scheduleAtFixedRate(new Runnable() {
        public void run() {    
            ZonedDateTime date = ZonedDateTime.now();            
            String time = DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm").format(date);
            currentState = "id: " + terminal_id + " pages: " + list_count + " time: " + time;
            listModel.addElement(currentState);     
            t1.setText("#"+terminal_id);
            p1.setText(""+list_count);
        }
    }, 0L, 60L, TimeUnit.SECONDS);
    }
    
}
